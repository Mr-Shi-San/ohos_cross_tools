wget https://github.com/jemalloc/jemalloc/releases/download/5.3.0/jemalloc-5.3.0.tar.bz2
tar xf jemalloc-5.3.0.tar.bz2
pushd jemalloc-5.3.0
./configure $CONFIGURE_ARGS --enable-prof --enable-stats --enable-debug && make && make install
popd
