wget https://ftp.gnu.org/gnu/coreutils/coreutils-9.0.tar.xz
tar xf coreutils-9.0.tar.xz
pushd coreutils-9.0/
./configure $CONFIGURE_ARGS && make && make install
popd
