wget https://curl.se/download/curl-7.80.0.tar.xz
tar xf curl-7.80.0.tar.xz
pushd curl-7.80.0
autoreconf -vfi &&
    ./configure $CONFIGURE_ARGS \
    --with-openssl \
    --without-libidn \
    --without-libidn2 \
    --disable-ldap \
    --with-pic \
    --without-libssh2 \
    --with-openssl=${DEBUGROOT} &&
    make && make install
popd
